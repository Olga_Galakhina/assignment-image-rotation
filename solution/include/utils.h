#ifndef UTILS_H
#define UTILS_H

#include "stdio.h"

enum open_status
{
    OPEN_OK,
    OPEN_ERROR
};

enum open_status file_open(char *filename, char *mode, FILE **file);

enum close_status
{
    CLOSE_OK,
    CLOSE_ERROR
};

enum close_status file_close(FILE *file);

#endif
