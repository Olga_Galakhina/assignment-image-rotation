#ifndef BMP_H
#define BMP_H

#include "stdint.h"
#include "stdio.h"
#include "image.h"


enum read_status
{
    READ_OK,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_ERROR
};

struct bmp_header generate_header(struct image const img);

enum read_status from_bmp(FILE *in, struct image *img);

enum read_status read_from_bmp(char *filename, struct image *img);

enum write_status
{
    WRITE_OK,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, struct image const img);

enum write_status write_to_bmp(char *filename, struct image const img);

#endif
