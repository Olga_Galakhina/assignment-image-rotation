#ifndef IMAGE_H
#define IMAGE_H

#include "stdint.h"

struct pixel
{
    uint8_t blue, green, red;
};

struct image
{
    uint64_t height;
    uint64_t width;
    struct pixel *data;
};

uint64_t get_pixel_offset(uint64_t width, uint64_t col, uint64_t row);

struct image init_image();

void close_image(struct image img);

#endif
