#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"

enum rotate_dir
{
    ROTATE_0,
    ROTATE_90,
    ROTATE_180,
    ROTATE_270
};

void pix_rotate_0(struct pixel* data, struct image const in, struct image* out);

void pix_rotate_90(struct pixel* data, struct image const in, struct image* out);

void pix_rotate_180(struct pixel* data, struct image const in, struct image* out);

void pix_rotate_270(struct pixel* data, struct image const in, struct image* out);

struct image rotate(struct image const in, enum rotate_dir dir);

#endif
