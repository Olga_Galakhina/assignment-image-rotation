#include "utils.h"
#include "stdio.h"

enum open_status file_open(char *filename, char *mode, FILE **in_file)
{
    *in_file = fopen(filename, mode);

    if (!*in_file)
        return OPEN_ERROR;

    return OPEN_OK;
}

enum close_status file_close(FILE *file)
{
    if (fclose(file) == EOF)
        return CLOSE_ERROR;

    return CLOSE_OK;
}
