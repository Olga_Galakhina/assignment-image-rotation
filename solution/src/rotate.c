#include "image.h"
#include "rotate.h"

#include "inttypes.h"
#include "stdlib.h"

void pix_rotate_0(struct pixel* data, struct image const in, struct image* out)
{
    out->height = in.height;
    out->width = in.width;
    for (size_t row = 0; row < in.height; row++)
    {
        for (size_t col = 0; col < in.width; col++)
        {
            data[get_pixel_offset(out->width, col, row)] = in.data[get_pixel_offset(in.width, col, row)];
        }
    }
}

void pix_rotate_90(struct pixel* data, struct image const in, struct image* out)
{
    out->height = in.width;
    out->width = in.height;
    for (size_t row = 0; row < in.height; row++)
    {
        for (size_t col = 0; col < in.width; col++)
        {
            data[get_pixel_offset(out->width, (out->width - 1 - row), col)] = in.data[get_pixel_offset(in.width, col, row)];
        }
    }
}

void pix_rotate_180(struct pixel* data, struct image const in, struct image* out)
{
    out->height = in.width;
    out->width = in.height;
    for (size_t row = 0; row < in.height; row++)
    {
        for (size_t col = 0; col < in.width; col++)
        {
            data[get_pixel_offset(out->width, (out->width - 1 - col), row)] = in.data[get_pixel_offset(in.width, col, row)];
        }
    }
}

void pix_rotate_270(struct pixel* data, struct image const in, struct image* out)
{
    out->height = in.width;
    out->width = in.height;
    for (size_t row = 0; row < in.height; row++)
    {
        for (size_t col = 0; col < in.width; col++)
        {
            data[get_pixel_offset(out->width, row, col)] = in.data[get_pixel_offset(in.width, col, row)];
        }
    }
}

struct image rotate(struct image const in, enum rotate_dir dir)
{
    struct image out = init_image();
    struct pixel *data = malloc(in.height * in.width * sizeof(struct pixel));
    switch (dir)
    {
    case ROTATE_90:
        // 90, против часовой стрелки
        pix_rotate_90(data, in, &out);
        break;
    case ROTATE_180:
        // 180, зеркально
        pix_rotate_180(data, in, &out);
        break;
    case ROTATE_270:
        // 270, против часовой стрелки
        pix_rotate_270(data, in, &out);
        break;
    default:
        // 0 / 360, Копия изображения
        pix_rotate_0(data, in, &out);
        break;
    }

    out.data = data;
    return out;
}
