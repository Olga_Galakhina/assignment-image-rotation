#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "utils.h"

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        printf("Check arguments!");
        return 1;
    }
    struct image input = init_image();

    if (read_from_bmp(argv[1], &input))
    {
        close_image(input);
        printf("Failed reading from BMP!");
        return 2;
    }

    struct image output = rotate(input, ROTATE_90);

    if (write_to_bmp(argv[2], output))
    {
        close_image(input);
        close_image(output);
        printf("Failed writing to BMP!");
        return 3;
    }

    close_image(input);
    close_image(output);
    return 0;
}
