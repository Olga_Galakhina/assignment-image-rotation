#include "image.h"
#include "stdlib.h"

uint64_t get_pixel_offset(uint64_t width, uint64_t col, uint64_t row)
{
    return row * width + col;
}

struct image init_image()
{
    return (struct image){0};
}

void close_image(struct image img)
{
    free(img.data);
}
