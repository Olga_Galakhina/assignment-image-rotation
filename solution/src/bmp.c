#include "bmp.h"
#include "image.h"
#include "stdio.h"
#include "stdlib.h"
#include "utils.h"

#define BM_TYPE 19778
#define BIT_COUNT 24
#define PIX_P_METER 2834

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t padding(const uint64_t width)
{
    return 4 - ((width * 3) % 4);
}

struct bmp_header generate_header(struct image const img)
{
    return (struct bmp_header){
        .bfType = BM_TYPE,
        .bfileSize = padding(img.width) * img.height + sizeof(struct bmp_header) + img.height * img.width * sizeof(struct pixel),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img.width,
        .biHeight = img.height,
        .biPlanes = 1,
        .biBitCount = BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = padding(img.width) * img.height + img.height * img.width * sizeof(struct pixel),
        .biXPelsPerMeter = PIX_P_METER,
        .biYPelsPerMeter = PIX_P_METER,
        .biClrUsed = 0,
        .biClrImportant = 0};
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    if (header.bfType != BM_TYPE)
        return READ_INVALID_SIGNATURE;

    img->height = header.biHeight;
    img->width = header.biWidth;

    // Переход к разделу данных
    if (fseek(in, header.bOffBits, 0))
        return READ_ERROR;

    // Выделение памяти на "пиксели"
    struct pixel *data = malloc(img->height * img->width * sizeof(struct pixel));
    if (!data)
    {
        free(data);
        return READ_ERROR;
    }
    //Чтение "пикселей"
    const uint8_t pad = padding(img->width);

    for (size_t row = 0; row < img->height; row++)
    {
        if (fread(&data[get_pixel_offset(img->width, 0, row)], sizeof(*data), img->width, in) != img->width)
            return READ_INVALID_BITS;
        if (fseek(in, pad, 1))
            return READ_ERROR;
    }

    img->data = data;

    return READ_OK;
}

enum read_status read_from_bmp(char *filename, struct image *img)
{
    FILE *file = {0};
    if (!file_open(filename, "r", &file))
    {
        const enum read_status status = from_bmp(file, img);

        if (file_close(file))
            return READ_ERROR;

        return status;
    }
    return READ_ERROR;
}

enum write_status to_bmp(FILE *out, struct image const img)
{
    struct bmp_header header = generate_header(img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    // Переход к разделу данных
    if (fseek(out, header.bOffBits, 0))
        return WRITE_ERROR;

    const uint8_t pad = padding(img.width);
    uint32_t pad_value = 0;

    for (size_t row = 0; row < img.height; row++)
    {
        if (fwrite(&img.data[get_pixel_offset(img.width, 0, row)], sizeof(struct pixel), img.width, out) != img.width)
            return WRITE_ERROR;
        if (fwrite(&pad_value, 1, pad, out) != pad)
            return WRITE_ERROR;
    }

    return WRITE_OK;
}

enum write_status write_to_bmp(char *filename, struct image const img)
{
    FILE *file = {0};
    if (!file_open(filename, "w+", &file))
    {
        const enum write_status status = to_bmp(file, img);

        if (file_close(file))
            return WRITE_ERROR;

        return status;
    }
    return WRITE_ERROR;
}
